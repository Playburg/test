﻿using Microsoft.Azure.Mobile.Server;

namespace playburg_test_mobile_appService.DataObjects
{
    public class TodoItem : EntityData
    {
        public string Text { get; set; }

        public bool Complete { get; set; }
    }
}