using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(playburg_test_mobile_appService.Startup))]

namespace playburg_test_mobile_appService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}